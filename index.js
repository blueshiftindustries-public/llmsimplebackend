import http from "http";
import { CopilotBackend, OpenAIAdapter } from "@copilotkit/backend";

const HEADERS = {
  // make sure to modify CORS headers to match your frontend's origin
  "Access-Control-Allow-Origin": "http://localhost:5174",
  "Access-Control-Allow-Methods": "POST, OPTIONS",
  "Access-Control-Allow-Headers": "Content-Type",
  "Access-Control-Max-Age": "86400", // Optional: Reduce the need for repeated pre-flight requests
};

const server = http.createServer((request, response) => {
  try {
    console.log(`${new Date().toISOString()} - ${request.method} request to ${request.url}`);

    if (request.headers["origin"] !== HEADERS["Access-Control-Allow-Origin"]) {
      console.log("Origin not allowed:", request.headers["origin"]);
      response.writeHead(403); // Forbidden access
      response.end("Forbidden: CORS policy does not allow access from your origin.");
      return;
    }

    if (request.method === "OPTIONS") {
      console.log("Handling OPTIONS request");
      // Respond to the OPTIONS request
      response.writeHead(204, HEADERS); // 204 No Content
      response.end();
      return;
    }
    const headers = {
      ...HEADERS,
      ...(request.method === "POST" && { "Content-Type": "application/json" }),
    };
    //console.log("req", request);
    //response.writeHead(200, headers);
    console.log("request.method", request.method);
    if (request.method == "POST") {
      let body = [];
      request
        .on("data", (chunk) => {
          body.push(chunk);
        })
        .on("end", () => {
          body = Buffer.concat(body).toString();
          console.log("POST body:", body);

          const headers = { ...HEADERS, "Content-Type": "application/json" };
          response.writeHead(200, headers);

          const copilotKit = new CopilotBackend();
          const openAIAdapter = new OpenAIAdapter();
          copilotKit.streamHttpServerResponse(request, response, openAIAdapter);
        });
    } else {
      console.log("get request");
      response.end("openai server");
    }
  } catch (err) {
    console.error(err);
    response.end("error");
  }
});

const port = 4201;
const host = "localhost";
server.listen(port, host);
console.log(`Listening at http://${host}:${port}`);
